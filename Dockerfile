FROM openjdk:12
ADD target/docker-spring-boot-lucas_viegas.jar docker-spring-boot-lucas_viegas.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-lucas_viegas.jar"]
